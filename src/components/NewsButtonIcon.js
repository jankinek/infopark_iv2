import React from "react";
import Badge from "@material-ui/core/Badge";
import NotificationsIcon from "@material-ui/icons/Notifications";
import IconButton from "@material-ui/core/IconButton";
import { api } from "../utils";

class NewsButtonIcon extends React.Component {
  state = { newNewsCount: 0 };

  async componentDidMount() {
    const { items } = await api.getAirportNews();
    const newNewsCount = this.calculateNewNewsCount(
      items,
      api.getLastestSeen()
    );

    api.updateLastestSeen(items[0].guid);
    this.setState({ newNewsCount });
  }

  

  calculateNewNewsCount(news, lastSeenGuid) {
    const index = news.findIndex(el => el.guid === lastSeenGuid);
    return index === -1 ? news.length : index;
  }

  render() {
    let { newNewsCount } = this.state;
    const isVisible = newNewsCount > 0;

    return (
      <IconButton color="inherit" >
        <Badge
          badgeContent={newNewsCount}
          invisible={!isVisible}
          color="secondary"
        >
          <NotificationsIcon />
        </Badge>
      </IconButton>
    );
  }
}

export default NewsButtonIcon;
