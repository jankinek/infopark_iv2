import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Hidden from "@material-ui/core/Hidden";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Grid from "@material-ui/core/Grid";
import TabContainer from "./TabContainer";
import NewsButtonIcon from "./NewsButtonIcon";

class TopBar extends React.Component {
  state = {
    value: 0,
    isVisible: false
  };
  tabs = ["Item One", "Item Two", "Item Three"];

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleClick = () => {
    this.setState((previousState, currentProps) => {
      return { isVisible: !previousState.isVisible };
    });
  };
  generateTabs = data => data.map(tab => <Tab key={tab} label={tab} />);

  render() {
    const { value, isVisible } = this.state;
    const tabs = this.generateTabs(this.tabs);
    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <Grid container xs={12}>
              <Hidden mdUp>
                <Grid item xs={1}>
                  <IconButton
                    color="inherit"
                    aria-label="Menu"
                    onClick={this.handleClick}
                  >
                    <MenuIcon />
                  </IconButton>
                </Grid>
              </Hidden>
              <Grid item xs={10}>
                <Hidden smDown>
                  <Tabs value={value} onChange={this.handleChange}>
                    {tabs}
                  </Tabs>
                </Hidden>
              </Grid>
              <Grid item xs={1}>
                <NewsButtonIcon />
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
        {isVisible && (
          <Hidden mdUp>
            <Tabs value={value} onChange={this.handleChange}>
              {tabs}
            </Tabs>
            {value === 0 && <TabContainer>Item One</TabContainer>}
            {value === 1 && <TabContainer>Item Two</TabContainer>}
            {value === 2 && <TabContainer>Item Three</TabContainer>}
          </Hidden>
        )}
        <Hidden smDown>
          {value === 0 && <TabContainer>Item One</TabContainer>}
          {value === 1 && <TabContainer>Item Two</TabContainer>}
          {value === 2 && <TabContainer>Item Three</TabContainer>}
        </Hidden>
      </div>
    );
  }
}

export default TopBar;
