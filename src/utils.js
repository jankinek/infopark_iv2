import {
  pressFeedDirnamePath,
  lastSeenNewsLocalStorageName
} from "./constants";
import rssParser from "rss-parser";
const parser = new rssParser();

const getFromLocalStorage = name => () => localStorage.getItem(name);
const updateLocalStorage = name => id => localStorage.setItem(name, id);
const getRSS = path => async () => await parser.parseURL(path);

const getLastestSeen = getFromLocalStorage(lastSeenNewsLocalStorageName);
const updateLastestSeen = updateLocalStorage(lastSeenNewsLocalStorageName);

const getAirportNews = getRSS(pressFeedDirnamePath);

export const api = {
  getAirportNews,
  updateLastestSeen,
  getLastestSeen
};
